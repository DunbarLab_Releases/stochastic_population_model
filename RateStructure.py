#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
RateStructure: Helper class that manipulates treatment schedules and their
effects for all the rates of a stochastic population model.

Created on Mon Apr 29 16:27:42 2019

@author: cordessf
"""

from bisect import bisect
import numpy as np
import Rate as rt

class RateStructure:
    '''
    add_treatment - method to add a treatment plan to an exisiting RateStructure
    '''
    def add_treatment(self,
                  start_time,
                  rate,
                  end_time = None,
                  affected_rate_type = 'All',
                  affected_initial_compartment = 'All',
                  affected_final_compartment = None,
                  affected_clonal_equivalence_class = 'All'):
        # Validation
        # ... start_time
        if start_time <= 0 or start_time > self.horizon:
            raise ValueError('[RateStructure::add_treatment] Cannot have treatment start time prior to t = 0 or after horizon.')
            
        # ... rate
        if rate < 0:
            raise ValueError('[RateStructure::add_treatment] Cannot have negative rate.')
        
        # ... end time
        if end_time <= start_time or end_time > self.horizon:
            raise ValueError('[RateStructure::add_treatment] Cannot have treatment end time prior to start time or after horizon.')
            
        # ... rate type
        if not (affected_rate_type in ['Apoptosis', 'Differentiation', 'Proliferation']) and \
            not (affected_rate_type == 'All'):
            raise ValueError('[RateStructure::add_treatment] Rate type not recognized.')
        
        # ... initial compartment
        if not (affected_initial_compartment in self.compartments) and \
            not (affected_initial_compartment == 'All'):
            raise ValueError('[RateStructure::add_treatment] Initial compartment not recognized.')
        
        # ... final compartment
        if not (affected_final_compartment in self.compartments) and \
            not (affected_final_compartment is None or affected_final_compartment == 'All'):
            raise ValueError('[RateStructure::add_treatment] Final compartment not recognized.')
        
        # ... clonal equivalence class
        if not (affected_clonal_equivalence_class in self.clonal_equivalence_classes) and \
            not (affected_clonal_equivalence_class == 'All'):
            raise ValueError('[RateStructure::add_treatment] Clonal equivalence structure not recognized.')
        
        # Manipulate the input data which potentially consists of 'All' or None
        # into explicit lists
        # ... clonal equivalence classes
        if affected_clonal_equivalence_class == 'All':
            clonal_classes = self.clonal_equivalence_classes
        elif type(affected_clonal_equivalence_class) == list:
            clonal_classes = affected_clonal_equivalence_class
        else:
            clonal_classes = [affected_clonal_equivalence_class]
        
        # ... rate types
        if affected_rate_type == 'All':
            rate_types = ['Apoptosis', 'Differentiation', 'Proliferation']
        elif type(affected_rate_type) == list:
            rate_types = affected_rate_type
        else:
            rate_types = [affected_rate_type]
            
        # ...initial compartments
        if affected_initial_compartment == 'All':
            initial_compartments = self.compartments
        elif type(affected_initial_compartment) == list:
            initial_compartments = affected_initial_compartment
        else:
            initial_compartments = [affected_initial_compartment]
        
        # ... final compartments
        if affected_final_compartment == 'All':
            final_compartments = self.compartments
        elif type(affected_final_compartment) == list:
            final_compartments = affected_final_compartment
        else:
            final_compartments = [affected_final_compartment]
            
        # Determine whether times need to be added to existing list of rate
        # change times
        # ... start_time
        if not start_time in self.times:
            start_idx = bisect(self.times, start_time)
            self.times.insert(start_idx, start_time)
        
        # ... end_time
        if not end_time in self.times:
            end_idx = bisect(self.times, end_time)
            self.times.insert(end_idx, end_time)
        
        # Add the actual treatment schedule to the rates
        for c in clonal_classes:
            for r in rate_types:
                for i_c in initial_compartments:
                    if r == 'Differentiation':
                        for f_c in final_compartments:
                            self.params[c][r][i_c][f_c].add_treatment( \
                                initial_rate = rate, \
                                start_time = start_time, \
                                end_time = end_time)
                    else:
                        self.params[c][r][i_c].add_treatment( \
                            apriori_rate = rate, \
                            start_time = start_time, \
                            end_time = end_time)
    
    '''
    alpha - method to retrieve apoptosis rates at a particular time across all
    compartments as a numpy vector
    '''
    def alpha(self,
              time,
              clonal_equivalence_class):
        # Validation
        if not clonal_equivalence_class in self.clonal_equivalence_classes:
            raise ValueError('[RateStructure::alpha] Unknown clonal equivalence class.')
            
        if not self.calculated:
            self.calculate()
        
        idx = self.time_to_idx(time)
        return self._alpha[idx][clonal_equivalence_class]
     
    '''
    compute_rates - method to compute RateStructure
    '''
    def compute_rates(self):
        # Clear previous rates
        self._alpha = []; self._delta = []; self._rho = []
        for u in self.times:
            alpha = dict(); delta = dict(); rho = dict()
            for cl in self.clonal_equivalence_classes: 
            
            # ... Apoptosis
                alpha[cl] = np.zeros(self.num_compartments)
                for co_0, r in self.params[cl]['Apoptosis'].items():
                    idx_0 = self.comp_idx[co_0]
                    alpha[cl][idx_0] = r.rate(u)
                self._alpha.append(alpha)
            
            # ... Differentiation
                delta[cl] = \
                    np.zeros([self.num_compartments, self.num_compartments])
                for co_0, kv_pair_0 in self.params[cl]['Differentiation'].items():
                    idx_0 = self.comp_idx[co_0]
                    for co_1, r in kv_pair_0.items():
                        idx_1 = self.comp_idx[co_1]
                        delta[cl][idx_0][idx_1] = r.rate(u)
                self._delta.append(delta)
            
            # ... Proliferation
                rho[cl] = np.zeros(self.num_compartments)
                for co_0, r in self.params[cl]['Proliferation'].items():
                    idx_0 = self.comp_idx[co_0]
                    rho[cl][idx_0] = r.rate(u)
                self._rho.append(rho)
            
            self.calculated = True
    
    '''
    delta - method to retrieve differentiation rates at a particular time
    across all compartments as a numpy vector
    '''
    def delta(self,
              time,
              clonal_equivalence_class):
        # Validation
        if not clonal_equivalence_class in self.clonal_equivalence_classes:
            raise ValueError('[RateStructure::delta] Unknown clonal equivalence class.')
            
        if not self.calculated:
            self.calculate()
        
        idx = self.time_to_idx(time)
        return self._delta[idx][clonal_equivalence_class]
    
    '''
    get_sparse_rates - accessor method to retrieve sparse rates [PROBABLY REQUIRES ANOTHER
    ITERATION]
    '''
    def get_sparse_rates(self):
        return self.params
    
    def __init__(self,
                 compartments,
                 clonal_equivalence_classes,
                 apriori_apoptosis_rates,
                 apriori_differentiation_rates,
                 apriori_proliferation_rates,
                 horizon):
        # Store away compartments in a set and define a dictionary between
        # compartment and integer index
        self.compartments = set()
        self.comp_idx = dict(); idx = 0
        for comp in iter(compartments):
            self.compartments.add(comp)
            self.comp_idx[comp] = idx
            idx += 1
        
        # Store away the number of compartments
        self.num_compartments = idx
            
        # Store away clonal equivalence classes in set
        self.clonal_equivalence_classes = set()
        for eq_class in iter(clonal_equivalence_classes):
            self.clonal_equivalence_classes.add(eq_class)
            
        self.params = dict()
        # Store away rates required for the stochastic population model
        # Note: any rates that are initially set to zero will remain zero
        # in the stochastic population model and will NOT be inferred.
        for c in clonal_equivalence_classes:
            self.params[c] = dict()
        
            # ... Apoptosis rates
            self.params[c]['Apoptosis'] = dict()
            for comp, rate in apriori_apoptosis_rates.items():
                if rate < 0:
                    raise ValueError('[StochasticPopulationModel] Negative apoptosis rate not permissible.')
                self.params[c]['Apoptosis'][comp] = \
                    rt.Rate(apriori_rate = rate,
                    rate_type = 'Apoptosis',
                    initial_compartment = comp)
        
            # ... Differentiation Rates
            self.params[c]['Differentiation'] = dict()
            for comp_0, kv_pair in apriori_differentiation_rates.items():
                self.params[c]['Differentiation'][comp_0] = dict()
                for comp_1, rate in kv_pair.items():
                    if rate < 0:
                        raise ValueError('[StochasticPopulationModel] Negative differentiation rate not permissible.')
                    self.params[c]['Differentiation'][comp_0][comp_1] = \
                        rt.Rate(apriori_rate = rate,
                                rate_type = 'Differentiation',
                                initial_compartment = comp_0,
                                final_compartment = comp_1)
        
            # ... Proliferation Rates
            self.params[c]['Proliferation'] = dict()
            for comp, rate in apriori_proliferation_rates.items():
                if rate < 0:
                    raise ValueError('[StochasticPopulationModel] Negative proliferation rate not permissible.')
                self.params[c]['Proliferation'][comp] = \
                    rt.Rate(apriori_rate = rate,
                            rate_type = 'Proliferation',
                            initial_compartment = comp)
        
        # Set the list of rate change times
        self.times = [0]
        
        # Set the horizon
        self.horizon = horizon
        
        # Intialize the rates - These will be represented as numpy arrays
        # associate with the time at which 
        # ... Apoptosis rates
        self._alpha = []
        
        # ... Differentiation rates
        self._delta = []
        
        # ... Proliferation rates
        self._rho = []
        
        # Set the calculational status flag
        self.calculated = False
    
    '''
    rho - method to retrieve proliferation rates at a particular time
    across all compartments as a numpy vector
    '''
    def rho(self,
            time,
            clonal_equivalence_class):
        # Validation
        if not clonal_equivalence_class in self.clonal_equivalence_classes:
            raise ValueError('[RateStructure::rho] Unknown clonal equivalence class.')
            
        if not self.calculated:
            self.calculate()
        
        idx = self.time_to_idx(time)
        return self._rho[idx][clonal_equivalence_class]
    
    '''
    set_sparse_rates - accessor method to set sparse rates [PROBABLY REQUIRES ANOTHER
    ITERATION]]
    '''
    def set_sparse_rates(self, sparse_rates):
        self.sparse_rates = sparse_rates
        
    def time_to_idx(self,
                    time):
        if time < 0:
            raise ValueError('[RateStructure::time_to_index] Cannot rate for time prior to t = 0.')
            
        if time in self.times:
            idx = self.times.index(time)
        else:
            idx = bisect(self.times, time) - 1
            
        return idx
    
    def __str__(self):
        if not self.calculated:
            self.compute_rates()
            
        serialization = '*** RateStructure ***'
        serialization += \
'''
Compartments:      {:s}
Rate Change Times: {:s}
'''.format(', '.join(self.compartments), ', '.join(map(str, self.times)))

        serialization += \
'''
*** Apoptosis Rates: ***
'''
        for t, alpha_dict in zip(self.times, self._alpha):
            serialization += \
'''
    t = {:3.0f}
    -------
'''.format(t)
            for cl, alpha_vec in alpha_dict.items():
                serialization += \
'''
        Clonal Equivalence Class: {:s}

        Compartment    Rate
        -----------    ----
'''.format(cl)
                for co in self.compartments:
                    serialization += \
                        '\t' + co + '\t\t' + \
                        str(alpha_vec[self.comp_idx[co]]) + '\n'
                        
        serialization += \
'''
*** Differentiation Rates: ***
'''
        for t, delta_dict in zip(self.times, self._delta):
            serialization += \
'''
    t = {:3.0f}
    -------
'''.format(t)
            for cl, delta_vec in delta_dict.items():
                serialization += \
'''

        Clonal Equivalence Class: {:s}

                       {:s}
        -----------
'''.format(cl, '\t'.join(self.compartments))
                for co_0 in self.compartments:
                    serialization += \
'''
    {:s} | {:s}'''.format(co_0, '\t'.join(map(str, delta_vec[self.comp_idx[co_0]])))
        
        serialization += \
'''

*** Proliferation Rates: ***
'''
        for t, rho_dict in zip(self.times, self._rho):
            serialization += \
'''
    t = {:3.0f}
    -------
'''.format(t)
            for cl, rho_vec in rho_dict.items():
                serialization += \
'''
        Clonal Equivalence Class: {:s}

        Compartment    Rate
        -----------    ----
'''.format(cl)
                for co in self.compartments:
                    serialization += '\t' + co + '\t\t' + \
                        str(rho_vec[self.comp_idx[co]]) + '\n'
        
        return serialization