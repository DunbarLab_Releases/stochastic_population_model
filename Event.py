#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Event class

Created on Fri Apr 12 11:12:51 2019

@author: cordessf
"""

import numpy as np

import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow_probability.python.internal import assert_util

'''
Event for stochastic population model is represented by (i) an event time,
(ii) an event type and (iii) the compartment in which the event occurs.  Since
this class is not using Tensors, it may not be amenable to acceleration by
TensorFlow.  Therefore the use of this class is DISCOURAGED; use the
Realization class instead.
'''
class Event:
    '''
    comp_0 - Accessor to the initial event compartment.
    '''
    def comp_0(self):
        return self._comp_0
    
    '''
    comp_1 - Accessor to the final even compartment.
    '''
    def comp_1(self):
        return self._comp_1
    
    '''
    __eq__ - Equality comparison for Events.
    '''
    def __eq__(self, event):
        return ((self._comp_0 == event.comp_0()) and \
                (self._comp_1 == event.comp_1()) and \
                (self._etime == event.etime()) and \
                (self._etype == event.etype()))
    
    '''
    etime - accessor to the event time
    '''
    def etime(self):
        return self._etime
    
    '''
    etype - accessor to the event type
    '''
    def etype(self):
        return self._etype
        
    '''
    __ge__ - Greater than or equal to comparison operator for Events compared
    to specified time.
    '''
    def __ge__(self, etime):
        return (self._etime >= etime)
    
    '''
    __gt__ - Greater than comparison for Events.
    '''
    def __gt__(self, event):
        return (self._etime > event.etime())
    
    '''
    Instantiate new event.
    '''
    def __init__(self, \
                 etime, \
                 etype, \
                 comp_0, \
                 comp_1 = ' ', \
                 validate_args = False):
        
        with tf.control_dependencies([
                assert_util.assert_non_negative(
                        etime, message = "Event time must be non-negative.")] if validate_args else []):
            self._etime = etime
            self._etype = etype
            self._comp_0 = comp_0
            self._comp_1 = comp_1
    
    '''
    __le__ - Less than or equal to comparison operator for Events compared to
    specified time.
    '''
    def __le__(self, etime):
        return (self._etime <= etime)
    
    '''
    __lt__ - Less than comparison for Events.
    '''
    def __lt__(self, event):
        return (self._etime < event.etime())
    
    '''
    __ne__ - Not equal to comparison operator for Events.
    '''
    def __ne__(self, event):
        return ((self._comp_0 != event.comp_0()) or \
                (self._comp_1 != event.comp_1()) or \
                (self._etime != event.etime()) or \
                (self._etype != event.etype()))
        
    # Serialization method
    def __str__(self):
        serialization = '*** Event ***'
        serialization += \
 '''
 Time:                {:f}
 Type:                {:s}
 Initial Compartment: {:s}
 Final Compartment:   {:s}
 '''.format(self.etime(), \
            self.etype(), \
            self.comp_0(), \
            self.comp_1())
 
        return serialization