#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Realization - Helper class to represent the realization of a stochastic
population model.  Due to the limitations of the Tensor class, this class
no longer relies on the Event helper class.  Backwards compatability in the
interfaces is preserved to the greatest extent possible.

The internal representation of a realization consists of:
    
(1) A representation of events:
    - Tensor of times, vector, dtype = tf.int32
    - Tensor of types, vector, dtype = tf.string
    - Tensor of comp_0, vector, dtype = tf.string
    - Tensor of comp_1, vector, dtype = tf.string

(2) A representation of initial population sizes:
    - Tensor of initial population sizes in each compartment, vector,
    dtype = tf.int32

(3) A representation of population sizes (immediately) after each event
    - Tensor of compartment population sizes immediately after each event,
      array, dtype = tf.int32

Created on Mon Apr 15 20:50:26 2019

@author: cordessf
"""

import numpy as np

import tensorflow as tf
import tensorflow_probability as tfp
tfd = tfp.distributions

class Realization:
    '''
    A - method to get number of apoptosis events, occurring between
    time u_0 and u_1.
    
    Arguments:
        u_0 - start time
        u_1 - end time
        
    Returns:
        Tensor specifying number of apoptosis events that occurred between
        time u_0 and u_1.
    '''
    def A(self, \
          u_0 = None, \
          u_1 = None, \
          event_type = 'Apoptosis'):
        return self.num_events_of_type(u_0 = u_0, \
                                       u_1 = u_1, \
                                       event_type = event_type)
    
    '''
    add_event - method to add a single event to a Realization
    '''
    def add_event(self, \
                  event_time, \
                  event_type, \
                  comp_0, \
                  comp_1 = ' '):
        if event_time == 'Differentiation' and comp_1 == ' ':
            raise ValueError('[Realization] Final compartments must be specified for events of Differentiation type.')
            
        # Set flag to indicate object needs to be recomputed
        self.calculated = False
        
        # Concatenate the event
        # ... Event Time
        self.event_times = tf.concat(self.event_times,
                                     tf.convert_to_tensor(value = [event_time], \
                                                          dtype = tf.int32))
        
        # ... Event Type
        self.event_types = tf.concat(self.event_types,
                                     tf.convert_to_tensor(value = [event_type], \
                                                          dtype = tf.string))
        
        # ... Initial Compartment
        self.comp_0 = tf.concat(self.comp_0,
                                tf.convert_to_tensor(value = [comp_0], \
                                                     dtype = tf.string))
        
        # ... Final Compartment
        self.comp_1 = tf.concat(self.comp_1,
                                tf.convert_to_tensor(value = [comp_1], \
                                                     dtype = tf.string))
        
        # Re-sort the list of events
        idx = np.argsort(self.event_times)
        self.event_times = self.event_times[idx]
        self.event_types = self.event_types[idx]
        self.comp_0 = self.comp_0[idx]
        self.comp_1 = self.comp_1[idx]

    '''
    add_event_list - method to add a list of events to a Realization.  Since
    this uses the DEPRECATED Event class, use of this method is DISCOURAGED.
    '''
    def add_event_list(self, \
                       event_list):
        # Set flag to indicate object needs to be recomputed
        self.calculated = False
        
        for e in iter(event_list):
            # Concatenate the event
            # ... Event Time
            self.event_times = tf.concat([ self.event_times,
                                           tf.convert_to_tensor(value = [e.etime()], \
                                                               dtype = tf.int32)], axis = 0)
        
            # ... Event Type
            self.event_types = tf.concat([ self.event_types,
                                           tf.convert_to_tensor(value = [e.etype()], \
                                                               dtype = tf.string)], axis = 0)
        
            # ... Initial Compartment
            self.comp_0 = tf.concat([ self.comp_0,
                                      tf.convert_to_tensor(value = [e.comp_0()], \
                                                          dtype = tf.string)], axis = 0)
        
            # ... Final Compartment
            self.comp_1 = tf.concat([ self.comp_1,
                                      tf.convert_to_tensor(value = [e.comp_1()], \
                                                          dtype = tf.string)], axis = 0)
        
        # Re-sort the list of events
        idx = np.argsort(self.event_times)
        self.event_times = tf.convert_to_tensor(value = self.event_times.numpy()[idx], \
                                                dtype = tf.int32)
        self.event_types = tf.convert_to_tensor(value = self.event_types.numpy()[idx], \
                                                dtype = tf.string)
        self.comp_0 = tf.convert_to_tensor(value = self.comp_0.numpy()[idx], \
                                           dtype = tf.string)
        self.comp_1 = tf.convert_to_tensor(value = self.comp_1.numpy()[idx], \
                                           dtype = tf.string)
        
    '''
    add_event - method to add a single event to a Realization
    '''
    def add_events(self, \
                  event_time_list, \
                  event_type_list, \
                  comp_0_list, \
                  comp_1_list = ' '):
        # Set flag to indicate object needs to be recomputed
        self.calculated = False
        
        # Concatenate the event
        # ... Event Time
        self.event_times = tf.concat(self.event_times,
                                     tf.convert_to_tensor(value = event_time_list, \
                                                          dtype = tf.int32))
        
        # ... Event Type
        self.event_types = tf.concat(self.event_types,
                                     tf.convert_to_tensor(value = event_type_list, \
                                                          dtype = tf.string))
        
        # ... Initial Compartment
        self.comp_0 = tf.concat(self.comp_0,
                                tf.convert_to_tensor(value = comp_0_list, \
                                                     dtype = tf.string))
        
        # ... Final Compartment
        self.comp_1 = tf.concat(self.comp_1,
                                tf.convert_to_tensor(value = comp_1_list, \
                                                     dtype = tf.string))
        
        # Re-sort the list of events
        idx = np.argsort(self.event_times)
        self.event_times = self.event_times[idx]
        self.event_types = self.event_types[idx]
        self.comp_0 = self.comp_0[idx]
        self.comp_1 = self.comp_1[idx]
        
    '''
    add_initial_populations - method to add the initial population sizes to the
    realization and extract the compartment names.
    
    Arguments:
        initial_population - a dictionary from compartment name to initial
        population size in that compartment
    '''
    def add_initial_populations(self, initial_populations):
        # Set flag to indicate that population size array is stale and will
        # need to be recomputed
        self.calculated = False
        
        # Initialize the tensor that represents the initial population size
        np_initial_sizes = np.zeros(self.num_compartments, dtype = np.int32)
        for c, s in initial_populations.items():
            if s < 0:
                raise ValueError('[Realization] Initial population sizes must be positive.')
            np_initial_sizes[self.comp_idx[c]] = s
            
        self.initial_populations = tf.convert_to_tensor(value = np_initial_sizes, \
                                                        dtype = tf.int32)
 
    '''
    add_rate_change_times - DEPRECATED - Now handled in StochasticPopulationModel
    '''
    def add_rate_change_times(self, event_list):
        pass
    
    '''
    compute_population_sizes - method to compute population sizes in all
    compartments immediately after each event
    '''
    def compute_population_sizes(self, \
                                 u_0 = None, \
                                 u_1 = None):
        # If u_0 is not specified, then set to 0
        if u_0 is None:
            u_0 = 0
        
        # If u_1 is not specified, then set to 1
        if u_1 is None:
            u_1 = self.horizon
        
        # Set the initial population size
        self.populations = [self.initial_populations]
        
        # Determine all events of type event_type between [inclusive] u_0 and
        # [exclusive] u_1
        included_idx = np.arange(len(self.event_types))[
                            np.logical_and(self.event_times.numpy() >= u_0, \
                                           self.event_times.numpy() < u_1)]
        
        previous_populations = self.initial_populations.numpy()
        for i in included_idx:
            changed_populations = self.population_changes(previous_populations = previous_populations, \
                                                          event_type = self.event_types.numpy()[i].decode(),
                                                          comp_0 = self.comp_0.numpy()[i].decode(),
                                                          comp_1 = self.comp_1.numpy()[i].decode())
            self.populations = tf.concat([self.populations, [changed_populations]], axis = 0)
            previous_populations = changed_populations
        
        # Set flag to indicate object has been recomputed
        self.calculated = True
    
    '''
    D - method to get number of differentiation events, occurring between
    time u_0 and u_1
    
    Arguments:
        u_0 - start time
        u_1 - end time
        
    Returns:
        Tensor specifying number of differentiation events that occurred
        between time u_0 and u_1.
    '''
    def D(self, \
          u_0 = None, \
          u_1 = None, \
          event_type = 'Differentiation'):
        return self.num_events_of_type(u_0 = u_0, u_1 = u_1, \
                                       event_type = event_type)
        
    '''
    delete_event - method to delete specified event from a Realization
    '''
    def delete_event(self, event):
        # Set flag to indicate object needs to be recomputed
        self.calculated = False

        # Delete the event
        self.events.remove(event)
        
    '''
    get_events - accessor to ordered events
    '''
    def get_events(self):
        return self.events
    
    '''
    Instantiates Realization
    
    Arguments:
        event_list - a list of Events
        initial_populations - a Tensor containing the initial population sizes
        in all compartments
    '''
    def __init__(self,
                 compartments,
                 horizon):
        # Set flag to indicate that population size array is stale and will
        # need to be recomputed
        self.calculated = False
        
        # Process internal representation of compartments
        # ... Compartments will be stored in tensor
        
        # ... Dictionary between index and compartment name
        self.comp_idx = dict()
        
        # ... Add the compartments
        self.compartments = tf.convert_to_tensor(value = [], dtype = tf.string)
        
        idx = 0
        for comp in iter(compartments):
            self.compartments = tf.concat([ self.compartments, \
                                            tf.convert_to_tensor(value = [comp], \
                                                                 dtype = tf.string) ], \
                                           axis = 0)
            self.comp_idx[comp] = idx
            idx += 1
        
        # Store away the number of compartments
        self.num_compartments = idx
        
        # Instantiate an empty event list
        # ... Event times
        self.event_times = tf.convert_to_tensor(value = [], dtype = tf.int32)
        
        # ... Event types
        self.event_types = tf.convert_to_tensor(value = [], dtype = tf.string)
        
        # ... Initial Compartment
        self.comp_0 = tf.convert_to_tensor(value = [], dtype = tf.string)
        
        # ... Final Compartment
        self.comp_1 = tf.convert_to_tensor(value = [], dtype = tf.string)
        
        # Add horizon
        self.horizon = horizon
            
    '''
    N - method to compute population sizes in all compartments over the time
    interval from u_0 to u_1
    '''
    def N(self,
          u_0 = None,
          u_1 = None):
        if not self.calculated:
            self.compute_population_sizes(u_0 = u_0, u_1 = u_1)
            
        return self.populations.numpy()
            
    '''
    num_events_of_type - method to get number of events of the type specified
    by event_type, occurring between time u_0 and u_1
    
    Arguments:
        u_0 - start time
        u_1 - end time
        
    Returns:
        Tensor specifying number of proliferation events that occurred
        between time u_0 and u_1.
    '''
    def num_events_of_type(self, \
                           event_type, \
                           u_0 = None, \
                           u_1 = None):
        # If u_0 is not specified, then set to 0
        if u_0 is None:
            u_0 = 0
        
        # If u_1 is not specified, then set to horizon
        if u_1 is None:
            u_1 = self.horizon
            
        if len(self.event_times) == 0:
            if event_type == "Differentiation":
                return np.zeros([self.num_compartments, self.num_compartments])
            else:
                return np.zeros([self.num_compartments])
            
        # Determine all events of type event_type between u_0 and u_1
        included_idx = np.logical_and(self.event_times >= int(u_0), \
                                      self.event_times < int(u_1))
        
        # Narrow to events of specified type
        included_idx = np.logical_and(included_idx, \
                                      self.event_types.numpy() == event_type.encode())
        
        included_comp_0 = self.comp_0[included_idx]
        
        if event_type == "Differentiation":
            results = np.zeros([self.num_compartments, self.num_compartments])
            
            included_comp_1 = self.comp_1[included_idx]
            for c0, c1 in zip(included_comp_0, included_comp_1):
                c_0 = c0.numpy().decode(); c_1 = c1.numpy().decode()
                if c_1 != ' ':
                    results[self.comp_idx[c_0]][self.comp_idx[c_1]] += 1
        else:
            results = np.zeros([self.num_compartments])
            for c0 in iter(included_comp_0):
                c_0 = c0.numpy().decode()
                results[self.comp_idx[c_0]] += 1
        
        return results
    
    '''
    population_changes - method to compute the population sizes in all
    compartments after a specified event.
    '''
    def population_changes(self, \
                           previous_populations, \
                           event_type,
                           comp_0,
                           comp_1):
        current_populations = np.copy(previous_populations)
        if event_type == "Apoptosis":
            current_populations[self.comp_idx[comp_0]] -= 1
        elif event_type == "Differentiation":
            current_populations[self.comp_idx[comp_0]] -= 1
            current_populations[self.comp_idx[comp_1]] += 1
        elif event_type == "Proliferation":
            current_populations[self.comp_idx[comp_0]] += 1
        
        return tf.convert_to_tensor(value = current_populations, dtype = tf.int32)
    
    '''
    R - method to get number of proliferation events, occurring between time 
    u_0 and u_1
    
    Arguments:
        u_0 - start time
        u_1 - end time
        
    Returns:
        Tensor specifying number of proliferation events that occurred
        between time u_0 and u_1.
    '''
    def R(self, \
          u_0 = None, \
          u_1 = None, \
          event_type = 'Proliferation'):
        return self.num_events_of_type(u_0 = u_0, u_1 = u_1, \
                                       event_type = event_type)
        
    '''
    S - method to compute the population lifetimes in all compartments
    '''
    def S(self, \
          u_0 = None,
          u_1 = None):
        # If u_0 is not specified, then set to 0
        if u_0 is None:
            u_0 = 0
        
        # If u_1 is not specified, then set to horizon
        if u_1 is None:
            u_1 = self.horizon
            
        '''
        Determine indices of all events with times [inclusively] between
        u_0 and u_1.
        '''
        included_idx = np.logical_and(self.event_times > int(u_0), \
                                      self.event_times < int(u_1))
        
        # Compute delta_t
        event_times = tf.concat([ tf.convert_to_tensor(value = [int(u_0)], \
                                                       dtype = tf.int32),
                                  self.event_times[included_idx], \
                                  tf.convert_to_tensor(value = [int(u_1)], \
                                                       dtype = tf.int32) ], \
                                axis = 0)
        delta_t = np.diff(event_times)
        
        # Compute population sizes at the begin of each time interval
        N = self.N(u_0, u_1)
        
        # Compute the total time lived by the population in each compartment
        return np.dot(np.transpose(N), delta_t)
        
    '''
    set_initial_populations - method that takes an initial population size,
    applies the events and computes the population sizes just after each event.
    '''
    def set_initial_populations(self, initial_populations):
        self.initial_populations = initial_populations
        
    def __str__(self):
        serialization = '*** Realization ***'
        serialization += \
'''
Number of Events: {:d}
 
Time\tType\t\t\tComp_0\t\tComp_1
----\t----\t\t\t------\t\t------
'''.format(len(self.event_times))
 
        # Serialize the events of the realization
        for ev_time, ev_type, comp_0, comp_1 in \
            zip(self.event_times.numpy(), self.event_types.numpy(), \
                self.comp_0.numpy(), self.comp_1.numpy()):
            serialization += \
'''
{:5.2f}\t{:s}\t\t{:s}\t\t{:s}'''.format(ev_time, ev_type.decode(), \
comp_0.decode(), comp_1.decode())
            
        return serialization