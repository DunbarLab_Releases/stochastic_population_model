#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
StochasticPopulationModel

StochasticPopulationModel is a class derived from Distribution.
    
Created on Fri Apr 12 16:49:30 2019

@author: cordessf
'''
import numpy as np
import RateStructure as rs
import Realization as re
import pandas

import tensorflow as tf
import tensorflow_probability as tfp
tfd = tfp.distributions
from tensorflow_probability.python.internal import assert_util

class StochasticPopulationModel(tfd.Distribution):
    '''
    add_clone_structure - method that sets up how many clones we are modelling
    We may also - temporarily have this method make an assignment of clone to
    parameter equivalence class (i.e. which clones respond to antigen)
    '''
    def add_clone_structure(self, clone_list):
        pass
    
    '''
    add_treatment - method that adds a time on which rates change to the
    piecewise constant rates.
    '''
    def add_treatment(self,
                      start_time,
                      rate,
                      end_time = None,
                      affected_rate_type = 'All',
                      affected_initial_compartment = 'All',
                      affected_final_compartment = None,
                      affected_clonal_equivalence_class = 'All'):
        # Add treatment as simple pass-through to the underlying RateSTructure
        self.rate_structure.add_treatment(start_time = start_time,
                                          rate = rate,
                                          end_time = end_time,
                                          affected_rate_type = affected_rate_type,
                                          affected_initial_compartment = affected_initial_compartment,
                                          affected_final_compartment = affected_final_compartment,
                                          affected_clonal_equivalence_class = affected_clonal_equivalence_class)
    
    '''
    get_initial_populations - accessor to get the initial population size of
    all the compartments
    '''
    def get_initial_compartments(self):
        pass
    
    '''
    Method that instantiates/initiatlizes a StochasticPopulationModel.
    '''
    def __init__(self,
                 compartments,
                 clonal_equivalence_classes,
                 apriori_apoptosis_rates,
                 apriori_differentiation_rates,
                 apriori_proliferation_rates,
                 horizon,
                 name = "StochasticPopulationModel",
                 seed = 42,
                 validate_args = False):
        
        with tf.name_scope(name) as name:
            # ... Extract name
            self.name = name
            
            # ... Set the seed for random number generation
            self._seed = seed
            
            # ... Extract dictionary from compartment names to integer indices
            self.comp_idx = dict(); idx = 0
            for comp in iter(compartments):
                self.comp_idx[comp] = idx
                idx = idx + 1
        
            # ... Note the number of compartments
            self.num_compartments = idx
            
            # ... Instantiate a RateStructure
            self.rate_structure = \
                rs.RateStructure(compartments = compartments,
                                 clonal_equivalence_classes = clonal_equivalence_classes,
                                 apriori_apoptosis_rates = apriori_apoptosis_rates,
                                 apriori_differentiation_rates = apriori_differentiation_rates,
                                 apriori_proliferation_rates = apriori_proliferation_rates,
                                 horizon = horizon)
            
            # ... Instantiate an empty list of realizations - will instantiate
            # real list once we have preprocessed input data to find out
            # - Identity and number of clones
            # - Likely initial population size of clone
            # - Clonal equivalence class of each clone
            self.realization = []

    '''
    log_prob - method that computes the log probability of the realization,
    given the parameters and data (Since the distribution of proposal
    realizations is asymmmetric, we will also have to add a correction via
    probability of new realization, given the old realization - this will 
    need to occur in StochasticPopulationModelTransitionKernel)
    '''
    def _log_prob(self):
        pass
    
    '''
    Mean realization is not interesting, but unbiased estimators of the rate
    constants from a realization would be useful.
    '''
    def _mean(self):
        pass

    '''
    Accessor to Distribution name.
    '''
    def name(self):
        return self.name
    
    '''
    Accessor to the number of compartments.
    '''
    def num_compartments(self):
        return self.num_compartments
    
    '''
    Input method for experimental data
    
    This method will call preprocessing methods to infer
        - Identity and number of clones
        - Likely initial population size of clone
        - Clonal equivalence class of each clone
    '''
    def read_data(self, metadata_file):
        pass
    
    '''
    sample_n - method to generate n random realizations, given the parameters
    '''
    def _sample_n(self, n = 1, seed = None):
        pass

    '''
    Accessor to random seed.
    '''
    def seed(self):
        return self._seed
    
    '''
    set_initial_populations - accessor to set the initial population size of
    all the compartments
    '''
    def set_initial_compartments(self, initial_populations):
        pass
    
    '''
    Standard deviation of a realization is not interesting.  Will probably not
    implement this method.
    '''
    def _stddev(self):
        pass
    
    '''
    Serialization method.
    '''
    def __str__(self):
        serialization = \
'''
Stochastic Population model:
===========================
'''
        # Serialize RateStructure
        serialization += str(self.rate_structure)
        
        # Serialize current Realization
        serialization += str(self.realization)
        
        return serialization
    
    '''
    Variance of a realization is not interesting.  Will likely leave this
    method unimplemented.
    '''
    def _variance(self):
        pass