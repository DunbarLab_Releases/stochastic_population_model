#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Rate: Helper class
    - Defines sparse structure for rates that are inferred via Bayesian
      inference
    - Defines method to convert from sparse to dense rate structures
    - Defines method to return numpy array of rates 

Created on Thu Apr 25 17:03:34 2019

@author: cordessf
"""

from bisect import bisect
import numpy as np

import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow_probability.python.internal import assert_util

'''
Rate - a helper class that manipulates treatment schedules and their effects
on a single rate.
'''
class Rate:
    '''
    add_treatment - method to add a treatment, which results in a rate change.
    The initial guess for the rate is initial_rate.  The rate change commences
    at time start_time.  If final_time is specified, then the rate reverts to
    the pre-treatment rate.
    '''
    def add_treatment(self,
                      apriori_rate,
                      start_time,
                      end_time = None,
                      validate_args = False):
        # Validation
        # Following is a limitation of the current implementation - things
        # would be a LOT more complicated, if we tried to loosen this restriction
        if start_time in self.times:
            raise ValueError('[Rate::add_treatment] Cannot overwrite previous treatment time.')

        with tf.control_dependencies([
                assert_util.assert_non_negative(
                        apriori_rate, message = "apriori_rate for rate changes must be non-negative."),
                assert_util.assert_non_negative(
                        start_time, message = "start_time for rate changes must be non-negative.")] if validate_args else []):
            start_idx = bisect(self.times, start_time)
        
            # Add the start treatment time to the (sorted) list of times
            self.times.insert(start_idx, start_time)
        
            # Add the the start rate change to the schedule
            self.schedule.insert(start_idx, len(self.sparse_rates))
        
            # Add the initial guess for the size of the rate to the sparse rates
            self.sparse_rates.append(apriori_rate)
        
        if not end_time is None:
            # Determine rate before commencement of treatment
            previous_schedule = self.schedule[start_idx - 1]
            end_idx = bisect(self.times, end_time)
            
            if end_idx - start_idx > 1:
                raise ValueError('[Rate::add_treatment] end_time runs over multiple treatment intervals.')
            
            # Add the start treatment time to the (sorted) list of times
            self.times.insert(end_idx, end_time)
        
            # Add the the start rate change to the schedule
            self.schedule.insert(end_idx, previous_schedule)
    
    '''
    get_sparse_rates - accessor to retrieve the full list of sparse rates.
    '''
    def get_sparse_rates(self):
        return self.sparse_rates
    
    '''
    Instantiates the class
    '''
    def __init__(self,
                 apriori_rate,
                 rate_type,
                 initial_compartment,
                 final_compartment = None,
                 validate_args = False):
        # Validation
        with tf.control_dependencies([
                assert_util.assert_non_negative(
                        apriori_rate, message = "a priori_rate for rate changes must be non-negative.")] if validate_args else []):
            # Initialize the treatment schedule - This translates between the dense
            # schedule of rates and the sparse rates - THIS DICTIONARY DOESN'T
            # CHANGE EVEN THOUGH SPARSE RATES ARE GETTING UPDATED ALL THE TIME.
            self.schedule = [0]
        
            # Initialize vector of sparse rates with the initial rate
            self.sparse_rates = [apriori_rate]
        
            # Initialize vector of rate change times with the initial time
            self.times = [0]
        
            # Initialize other factors specific to rate - rate type, initial/final
            # compartment - Strictly speaking these are extraneous.
            self.initial_compartment = initial_compartment
            self.final_compartment = final_compartment
            self.rate_type = rate_type
    
    '''
    rate - method to retrieve the rate at time t
    '''
    def rate(self, time):
        # Look up index corresponding to time in schedule
        idx = self.time_to_idx(time)
        
        # Return the rate from th
        return self.sparse_rates[self.schedule[idx]]
    
    '''
    set_sparse_rates - accessor to set the full set of sparse rates.
    '''
    def set_sparse_rates(self, sparse_rates):
        # Requires validation of data being set - should not be different
        # dimension
        
        if len(self.sparse_rates) != len(sparse_rates):
            raise ValueError('[Rate::set_sparse_rates] Attempted to set sparse rates with array of incompatible size.')
            
        self.sparse_rates = sparse_rates
    
    '''
    serialization method
    '''
    def __str__(self):
        serialization = '*** Event ***'
        
        if self.final_compartment is None:
            serialization += \
'''

Rate Type:   {:s}
Compartment: {:s}'''.format(self.rate_type, self.initial_compartment, end = '')
        else:
            serialization += \
'''

Rate Type:           {:s}
Initial Compartment: {:s}
Final Compartment:   {:s}'''.format(self.rate_type, \
self.initial_compartment, self.final_compartment, end = '')

        serialization += \
'''

Sparse rates: {:s}
'''.format(', '.join(map(str, self.sparse_rates)), end = '')

        serialization += \
'''
Time\tIndex\tRate
----\t-----\t----'''.format(end = '')

        for i in np.arange(len(self.times)):
            idx = self.schedule[i]
            serialization += \
'''
{:5.2f}\t{:5d}\t{:5.2f}'''.format(self.times[i], idx , self.sparse_rates[idx], \
end = '')

        return serialization
    
    def time_to_idx(self,
                    time):
        if time < 0:
            raise ValueError('[Rate::time_to_index] Cannot rate for time prior to t = 0.')
            
        if time in self.times:
            idx = self.times.index(time)
        else:
            idx = bisect(self.times, time) - 1
            
        return idx