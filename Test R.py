#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test R.py

Created on Fri Apr 26 14:09:22 2019

@author: cordessf
"""

import numpy as np
import Rate as r
import RateStructure as rs

print("******************************************************************")
print("Demonstration of instantiating and serializing a single rate.")

rate_1 = r.Rate(apriori_rate = 10.0,
                rate_type = 'Apoptosis',
                initial_compartment = 'LT-HSC',
                final_compartment = None)

print(rate_1)
print("******************************************************************",
      end = '\n\n\n')

print("******************************************************************")
print("Demonstration of adding a simple treatment schedule.")
rate_1.add_treatment(apriori_rate = 20.0,
                     start_time = 10.0,
                     end_time = 20.0)

print(rate_1)
print("******************************************************************",
      end = '\n\n\n')

print("******************************************************************")
print("Demonstration of adding more treatments.")
rate_1.add_treatment(apriori_rate = 30.0,
                     start_time = 30.0,
                     end_time = 40.0)

print(rate_1)
print("******************************************************************",
      end = '\n\n\n')

print("******************************************************************")
print("Demonstration of retrieving rates at particular times.")
for t in np.arange(50):
    r = rate_1.rate(t)
    print('Time = ' + str(t) + ' -- Rate = ' + str(r))
print("******************************************************************",
      end = '\n\n\n')

print("******************************************************************")
print("Demonstration of instantiating a RateStructure object.\n")

'''
Specify the compartment structure
'''
compartments = {'LT-HSC',
                'ST-HSC',
                'CD16m',
                'CD16dim',
                'CD16p',
                'Lymph'}

'''
Clonal Equivalence Classes
'''
clonal_equivalence_classes = {'Antigen Unresponsive',
                              'Antigen Responsive'}

'''
Set a priori rates.  Note: only rates that are initially non-vanishing will
be inferred.

A priori apoptosis rates in each compartment.
'''
apriori_apoptosis_rates = {'LT-HSC' : 0,
                           'ST-HSC' : 0,
                           'CD16m'  : 10,
                           'CD16dim': 10,
                           'CD16p'  : 10,
                           'Lymph'  : 10}

'''
A priori differentiation rates between compartments.  Note: since only rates
that are initially non-vanishing will be inferred, this *fixes* the topology
of how developmental processes play out.
'''
apriori_differentiation_rates = {'LT-HSC'  : {'ST-HSC'  :  5},
                                 'ST-HSC'  : {'CD16m'   : 10, 'Lymph' : 10},
                                 'CD16m'   : {'CD16dim' : 10},
                                 'CD16dim' : {'CD16p'   :  5}}

'''
A priori proliferation rates in each compartment.  Bayesian inference will only
be used to infer rates that are *initially* non-vaishing.
'''
apriori_proliferation_rates = {'LT-HSC' : 10,
                               'ST-HSC' : 10,
                               'CD16m'  : 5,
                               'CD16dim': 5,
                               'CD16p'  : 0,
                               'Lymph'  : 5}


# Instantiate the RateStructure object
rate_structure = rs.RateStructure(compartments = compartments,
                                  clonal_equivalence_classes = clonal_equivalence_classes,
                                  apriori_apoptosis_rates = apriori_apoptosis_rates,
                                  apriori_differentiation_rates = apriori_differentiation_rates,
                                  apriori_proliferation_rates = apriori_proliferation_rates,
                                  horizon = 100)

print(rate_structure)
print("******************************************************************",
      end = '\n\n\n')

rate_structure.add_treatment(start_time = 5.0,
                             rate = 15.0,
                             end_time = 6.0,
                             affected_rate_type = 'Proliferation',
                             affected_initial_compartment = 'Lymph')