#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 16:07:18 2019

@author: cordessf
"""

import StochasticPopulationModel as spm

'''
Specify the compartment structure
'''
compartments = {'LT-HSC',
                'ST-HSC',
                'CD16m',
                'CD16dim',
                'CD16p',
                'Lymph'}

'''
Set the initial guesses for all rates.  Note: only rates that are initially
non-vanishing will be inferred.

Apoptosis rates in each compartment.
'''
apoptosis_rates = {'LT-HSC' : 0,
                   'ST-HSC' : 0,
                   'CD16m'  : 10,
                   'CD16dim': 10,
                   'CD16p'  : 10,
                   'Lymph'  : 10}

'''
Differentiation rates between compartments.  Note: since only rates that are
initially non-vanishing will be inferred, this *fixes* the topology of how
developmental processes play out.
'''
differentiation_rates = {'LT-HSC'  : {'ST-HSC'  :  5},
                         'ST-HSC'  : {'CD16m'   : 10, 'Lymph' : 10},
                         'CD16m'   : {'CD16dim' : 10},
                         'CD16dim' : {'CD16p'   :  5}}

'''
Proliferation rates in each compartment.  Bayesian inference will only be used
to infer rates that are *initially* non-vaishing.
'''
proliferation_rates = {'LT-HSC' : 10,
                       'ST-HSC' : 10,
                       'CD16m'  : 5,
                       'CD16dim': 5,
                       'CD16p'  : 0,
                       'Lymph'  : 5}

'''
Instantiate the Stochastic Population Model
'''
SPM = spm.StochasticPopulationModel(compartments = compartments,
                                    clonal_equivalence_classes = {'Antigen Responsive',
                                                                  'Antigen Unresponsive'},
                                    apriori_apoptosis_rates = apoptosis_rates,
                                    apriori_differentiation_rates = differentiation_rates,
                                    apriori_proliferation_rates = proliferation_rates,
                                    horizon = 100)

'''
Serialize the Stochastic Population Model
'''
print(SPM)