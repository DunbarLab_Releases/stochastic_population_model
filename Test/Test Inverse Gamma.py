#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test Inverse Gamma Distribution

Created on Thu May  2 14:24:54 2019

@author: cordessf
"""
import numpy as np
from scipy import stats

import tensorflow as tf
import tensorflow_probability as tfp

tfd = tfp.distributions

# tf.enable_eager_execution()

inv_gamma = tfd.InverseGamma(concentration = 3.0, rate = tf.constant(1.0))

x = inv_gamma.sample(int(1e4)).numpy()

for i in np.arange(10000):
    print(x[i])