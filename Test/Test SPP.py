#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
SPM Test Harness

Created on Fri Apr 12 11:32:29 2019

@author: cordessf
"""

'''
Event for stochastic population model is represented by (i) an event time,
(ii) an event type and (iii) the compartment in which the event occurs.
'''

import Event as ev
import numpy as np
import Realization as re

# Tests
# ... Instantiate some events
event_1 = ev.Event(etime = 1, \
                   etype = 'Proliferation',
                   comp_0 = 'LT-HSC')
event_2 = ev.Event(etime = 2, \
                   etype = 'Differentiation',
                   comp_0 = 'LT-HSC',
                   comp_1 = 'ST-HSC')
event_3 = ev.Event(etime = 3, \
                   etype = 'Proliferation',
                   comp_0 = 'ST-HSC')
event_4 = ev.Event(etime = 4, \
                   etype = 'Proliferation',
                   comp_0 = 'ST-HSC')
event_5 = ev.Event(etime = 5, \
                   etype = 'Differentiation',
                   comp_0 = 'ST-HSC',
                   comp_1 = 'CD16m')
event_6 = ev.Event(etime = 6, \
                   etype = 'Differentiation',
                   comp_0 = 'ST-HSC',
                   comp_1 = 'Lymph')
event_7 = ev.Event(etime = 7, \
                   etype = 'Proliferation',
                   comp_0 = 'CD16m')
event_8 = ev.Event(etime = 8, \
                   etype = 'Differentiation',
                   comp_0 = 'CD16m',
                   comp_1 = 'CD16dim')
event_9 = ev.Event(etime = 9, \
                   etype = 'Proliferation',
                   comp_0 = 'CD16dim')
event_10 = ev.Event(etime = 10, \
                    etype = 'Differentiation',
                    comp_0 = 'CD16dim',
                    comp_1 = 'CD16p')

# Test Event serialization method
print("******************************************************************")
print("Demonstration of Event serialization method:\n")
print(event_1)
print("******************************************************************\n\n")

# Test overloaded comparison operator
print("******************************************************************")
print("Demonstration of the effects of ordering operations on events:\n")
print("event_2 > event_1:" + "\t" + str(event_2 > event_1) + "\n")
print("******************************************************************\n\n")

# Create numpy array events

events = np.array([event_1, event_4, event_3, event_2, event_10, \
                   event_9, event_8, event_7, event_6, event_5])

# Print numpy array of events
print("******************************************************************")
print("Demonstration of serialization of numpy array of Events:\n")
for e in iter(events):
    print(e)
print("******************************************************************\n\n")

# Sort list of events
events.sort()

# Compare the events to fimes
print("******************************************************************")
print("Demonstration of subsetting Events:\n")
print("This picks out all events between times 0 and 1.5.\n")
print(np.logical_and(events >= 0, events <= 1.5))
print("******************************************************************\n\n")

# Print sorted list of events
print("******************************************************************")
print("Sorted numpy array of Events:\n")
for e in iter(events):
    print(e)
print("******************************************************************\n\n")
    
# Enumerate the compartments
compartments = ('LT-HSC', 'ST-HSC', 'CD16m', 'CD16dim', 'CD16p', 'Lymph')

# Instantiate a new Realization
realization = re.Realization(compartments = compartments, \
                             horizon = 10.0)

# Add a list of events
realization.add_event_list(events)

initial_populations = {'LT-HSC' : 1, 'ST-HSC' : 0, 'CD16m' : 0, 'Lymph' : 0,
                       'CD16dim': 0, 'CD16p' : 0}

realization.add_initial_populations(initial_populations)

print("******************************************************************")
print("Demonstration of Realization serialization method:\n")
# Print the Realization
print(realization)
print("******************************************************************\n\n")

print("******************************************************************")
print("Demonstration of ability to compute total number of apoptotic events:\n")
print(realization.A())
print("******************************************************************\n\n")

print("******************************************************************")
print("Demonstration of ability to compute number of apoptotic events in ",
      end = '')
print("each compartment (between time 0 and time 2):\n")
print(realization.A(u_0 = 0, u_1 = 2))
print("******************************************************************\n\n")

print("******************************************************************")
print("Demonstration of ability to compute total number of differentiation events:\n")
print(realization.D())
print("******************************************************************\n\n")

print("******************************************************************")
print("Demonstration of ability to compute number of differentiation events in ",
      end = '')
print("each compartment (between time 0 and time 2):\n")
print(realization.D(u_0 = 0, u_1 = 2))
print("******************************************************************\n\n")

print("******************************************************************")
print("Demonstration of ability to compute total number of proliferation events:\n")
print(realization.R())
print("******************************************************************\n\n")

print("******************************************************************")
print("Demonstration of ability to compute number of proliferation events in ",
      end = '')
print("each compartment (between time 0 and time 2):\n")
print(realization.R(u_0 = 0, u_1 = 2))
print("******************************************************************\n\n")

print("******************************************************************")
print("Demonstration of ability to compute population sizes: \n")
print(realization.N())
print("******************************************************************\n\n")

print("******************************************************************")
print("Demonstration of ability to compute population lifetimes: \n")
print(realization.S())
print("******************************************************************\n\n")