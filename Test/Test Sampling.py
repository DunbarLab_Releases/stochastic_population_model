#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  6 16:38:07 2019

@author: cordessf
"""

import numpy as np
import Event as ev

import tensorflow as tf
import tensorflow_probability as tfp

tfd = tfp.distributions

# tf.enable_eager_execution()
low_thresh = 1e-10
runif = tfd.Uniform(low = low_thresh)

num_clones = 1e2
num_compart = 6
num_expected_events = 10
num_samples = 1e4

print('**********************************************************************')
print('Generating uniform random samples ...')
u = runif.sample([int(num_samples), int(num_clones), int(num_expected_events)]).numpy()
print('Done.')
print('**********************************************************************')
print('')

print('')
print('**********************************************************************')
print('Example of multiple samples of draws from uniform distribution' + \
      'for multiple clones ... ')
for i in np.arange(num_expected_events):
    print(u[i])
print('Done.')
print('**********************************************************************')
print('')

print(np.sum(u == 0))

if(np.any(u == 0)):
    print('At least one element of u is vanishing ... and will cause problems with log.')

# Convert into event times
t = -np.log(u)

print('')
print(t)
print('')

# Trial of very simple numeric structured array
str_array_1 = np.array([1, 2, 3, 4], dtype = np.uint)
event_1 = tf.convert_to_tensor(str_array_1, dtype = tf.uint8)
x_1 = event_1.numpy()

print(event_1)
print(x_1)
print(x_1[0])
print(x_1[1])
print(x_1[2])
print(x_1[3])