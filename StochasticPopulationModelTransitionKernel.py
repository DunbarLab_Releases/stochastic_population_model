#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
StochasticPopulationModelTransitionKernel is a class derived from
TransitionKernel and returns a new state given some old state.  It also returns
some side information.

Notes:
    - The method one_step should be able to update multiple chains in parallel
      It assumes that all left-most dimensions of the 'current_state' index
      independent chain states and are therefore updated independently.
    - The output of 'target_log_prob_fn(*current_state)' should sum
      log-probabilities across all event dimensions.
    - The number of independent chains is
      tf.size(target_log_prob_fn(*current_state))

Created on Tue Apr 16 09:53:57 2019

@author: cordessf
"""

import numpy as np

import tensorflow as tf
import tensorflow_probability as tfp
tfd = tfp.distributions

from tensorflow_probability.python.mcmc import kernel as kernel_base

class StochasticPopulationModelTransitionKernel(kernel_base.TransitionKernel): 
    '''
    boot_strap_results - method that returns an object with the same type as
    returned by one_step(...)[1].
    
    Arguments:
    init_state: Tensor or python list of Tensors representing the initial
    state(s) of the Markov chain(s)
    
    Returns:
    kernel_results: A (possibly nested) 'tuple', 'namedtupe' or python list of
    Tensors representing internal calculations made within the previous call to
    this function (or as returned by 'bootstrap_results').
    '''
    def bootstrap_results(self, init_state):
        return kernel_results
    
    '''
    compute_parameter_update - method to compute update to parameters for
    underlying stochastic population model.
    '''
    def compute_parameter_update(self):
        # Decide what parameters need to be generated  - Those that are set
        # to zero at the outset will not be generated.
        
        # For each clonal equivalence class
        #   For each inferred rate parameter
        #       For each treatment interval                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
        # ... Retrieve current hyperparameters (a's and b's)
        
        # ... Compute numbers of apoptotic, differentiatinn and proliferation
        #     events in each compartment
        
        # ... Draw new set of hyperparameters from nverse Gamma disribution
        #     with shifted shape parameters a_i -> a_i + A_i
        pass
    
    '''
    compute_realization_update - method to compute update to current
    realization.
    '''
    def compute_realization_update(self):
        pass
    
    '''
    get_hyperparameters - acessor to hyperparameters, i.e. parameters for the
    prior distribution.
    '''
    def get_hyperparameter(self):
        pass
    
    '''
    Method that instantiates/initiatlizes a StochasticPopulationModel.
    We need to specify:
        
        compartment_names - A set of compartment names
        
        initial_apoptosis_rates - A dictionary of compartment names to initial
        apoptosis rates
        
        initial_apoptosis_time_structure - A dictionary from time to positive
        real number representing the multiplier applied to the initial
        apoptosis rates after the preceding time point specified in the time
        structure
        
        initial_differentiation_rates - A dictionary of beginning and ending
        compartment names to initial differentiation rates
        
        initial_differentiation_rate_time_structure - A dictionary from time to
        positive real number representing the multiplier applied to the initial
        differentiation rates after the preceding time point specified in the
        time structure
        
        initial_self_renewal_rates - A dictionary of compartment names to
        initial self-renewal rates
        
        initial_self_renewal_rate_time_structure - A dictionary from time to
        positive real number representing the multiplier applied to the initial
        differentiation rates after the preceding time point specified in the
        time structure
        
    Internally it generates a representation of the model using integer indices
    
        A dictionary of compartment names to integer indices
        
        A matrix that represents a mapping from a pair of integer indices
        (representing the beginning and ending compartment of a differentiation
        process) to 
        
    '''
    def __init__(self,
                 compartment_names,
                 initial_apoptosis_rates,
                 initial_differentiation_rates = None,
                 initial_self_renewal_rates = None,
                 initial_apoptosis_time_structure = None,
                 initial_differentiation_rate_time_structure = None,
                 initial_self_renewal_rate_time_structure = None):
        # ... Extract dictionary from compartment names to integer indices
        self.comp_idx = dict(); idx = 0
        for comp in iter(compartment_names):
            self.comp_idx[comp] = idx
            idx = idx + 1
            
        self.num_compartments = idx
            
        # ... Extract initial apoptosis rates into numpy array
        self.initial_apoptosis_rates = np.zeros(self.num_compartments)
        for comp, rate in initial_apoptosis_rates.items():
            idx = self.comp_idx[comp]
            self.initial_apoptosis_rates[idx] = rate
            
        # ... Extract initial differentiation rates
        num_diff_transitions = 0
        self.initial_differentiation_rates = \
            np.zeros([self.num_compartments, self.num_compartments])
        for comp_0, kv_pair in initial_differentiation_rates.items():
            for comp_1, rate in kv_pair.items():
                idx0 = self.comp_idx[comp_0]
                idx1 = self.comp_idx[comp_1]
                self.initial_differentiation_rates[idx0][idx1] = rate
            
        # ... Extract initial self-renewal rates into numpy array
        self.initial_self_renewal_rates = np.zeros(self.num_compartments)
        for comp, rate in initial_self_renewal_rates.items():
            idx = self.comp_idx[comp]
            self.initial_self_renewal_rates[idx] = rate
            
    def is_calibrated(self):
        pass
    
    '''
    Accessor to TransitionKernel name.
    '''
    def name(self):
        pass
    
    '''
    one_step - method that takes one step of the TransitionKernel
    
    Arguments:
    current_state: Tensor or python list of Tensors representing the current
    state(s) of the Markov chain(s)
    
    previous_kernel_results: A (possibly nested) 'tuple', 'namedtupe' or
    python list of Tensors representing internal calculations made within
    the previous call to this function (or as returned by 'bootstrap_results').
    
    Returns:
    next_state: ensor or python list of Tensors representing the next state(s)
    of the Markov chain(s).
    
    kernel_results: A (possibly nested) 'tuple', 'namedtupe' or python list of
    Tensors representing internal calculations made within the previous call to
    this function (or as returned by 'bootstrap_results').
    '''
    def one_step(self,
                 current_state,
                 previous_kernel_results):
        '''
        This encodes the full Gibbs sampling:
            - Parameter update in the first step
            - Realization update in the second step
        '''
        
        # Compute the parameter update
        self.compute_parameter_update()
        
        # Computethe realization update
        self.compute_realization_update()
        
        return self.next_state, self.kernel_results
    
    '''
    Accessor to random seed.
    '''
    def seed(self):
        pass
    
    '''
    set_hyperparameters - accessor to set the hyperparameters, i.e. parameters
    that specify the prior distribution.
    '''
    def set_hyperparameters(self):
        pass
    
    '''
    Serialization method.
    '''
    def __str__(self):
        serialization = \
'''
Stochastic Population Model Transition Kernel:
=============================================

Compartment\tIndex\tApo Rate\tSelf-Renew
-----------\t-----\t--------\t----------
'''
        for comp, idx in self.comp_idx.items():
            serialization += comp + "\t\t" + str(idx) + "\t" + \
                str(self.initial_apoptosis_rates[idx]) + "\t\t" + \
                str(self.initial_self_renewal_rates[idx]) + "\n"

        serialization += \
'''
Differentiation Rates:
---------------------
'''
        
        serialization += '\t' + \
            '\t'.join(map(str, np.arange(self.num_compartments))) + '\n'

        for idx0 in np.arange(self.num_compartments):
            serialization += str(idx0) + "\t" + \
                '\t'.join(map(str, self.initial_differentiation_rates[idx0])) + '\n'
            
        return serialization